CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration

INTRODUCTION
------------

The *Taxonomy Internal* module allows taxonomy vocabularies to be marked as
"internal". When a vocabulary is marked internal, the canonical page of its
terms is:

 * only accessible to users that are allowed to update the term
 * displayed using the administrative theme

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the Taxonomy Internal module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------
Go to *Structure* -> *Taxonomy* and Edit a vocabulary to mark it as "internal".
