<?php

namespace Drupal\taxonomy_internal\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\VocabularyInterface;

class InternalTermThemeNegotiator implements ThemeNegotiatorInterface {

  protected AccountInterface $user;
  protected ConfigFactoryInterface $configFactory;

  public function __construct(AccountInterface $user, ConfigFactoryInterface $config_factory) {
    $this->user = $user;
    $this->configFactory = $config_factory;
  }

  public function applies(RouteMatchInterface $route_match): bool {
    if (!$this->user->hasPermission('view the administration theme')
      || !$route_match->getRouteName() == 'entity.taxonomy_term.canonical') {
      return FALSE;
    }

    $term = $route_match->getParameter('taxonomy_term');
    if (!$term instanceof TermInterface) {
      return FALSE;
    }

    $vocabulary = $term->get('vid')->entity;
    assert($vocabulary instanceof VocabularyInterface);

    return (bool) $vocabulary->getThirdPartySetting('taxonomy_internal', 'internal', FALSE);
  }

  public function determineActiveTheme(RouteMatchInterface $route_match): ?string {
    return $this->configFactory->get('system.theme')->get('admin');
  }

}
