<?php

namespace Drupal\taxonomy_internal\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\VocabularyInterface;

class TaxonomyInternalAccessCheck implements AccessInterface {

  /**
   * @see \Drupal\Core\Access\AccessManager::check()
   *   Route access checks are being combined with AND, and thus should either
   *   return allowed or forbidden.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account): AccessResultInterface {
    $term = $route_match->getParameter('taxonomy_term');

    if (!$term instanceof TermInterface) {
      return AccessResult::allowed();
    }

    $vocabulary = $term->get('vid')->entity;
    assert($vocabulary instanceof VocabularyInterface);

    if (!$vocabulary->getThirdPartySetting('taxonomy_internal', 'internal', FALSE)) {
      return AccessResult::allowed()
        ->addCacheableDependency($vocabulary);
    }

    $update_access = $term->access('update', $account, TRUE);

    if ($update_access->isAllowed()) {
      return AccessResult::allowed()
        ->addCacheableDependency($update_access)
        ->addCacheableDependency($vocabulary);
    }

    return AccessResult::forbidden()
      ->addCacheableDependency($update_access)
      ->addCacheableDependency($vocabulary);
  }

}
