<?php

namespace Drupal\taxonomy_internal\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class AlterTermCanonicalRouteSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.taxonomy_term.canonical')) {
      $route->setRequirement('_taxonomy_internal_canonical_access', 'TRUE');
    }
  }

}
