<?php

namespace Drupal\taxonomy_internal\Form;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\VocabularyInterface;

class VocabularyFormExtension {

  use StringTranslationTrait;

  public function alter(array &$form, FormStateInterface $form_state): void {
    $form_object = $form_state->getFormObject();
    assert($form_object instanceof EntityFormInterface);

    $vocabulary = $form_object->getEntity();
    assert($vocabulary instanceof VocabularyInterface);

    $form['taxonomy_internal'] =[
      '#type' => 'checkbox',
      '#title' => $this->t('Internal vocabulary'),
      '#description' => $this->t('If enabled, access to canonical routes of terms are forbidden.'),
      '#default_value' => $vocabulary->getThirdPartySetting('taxonomy_internal', 'internal', FALSE),
    ];

    $form['#entity_builders'][] = [$this, 'buildEntity'];
  }

  public function buildEntity(string $entity_type, VocabularyInterface $vocabulary, array $form, FormStateInterface $form_state): void {
    $vocabulary->setThirdPartySetting('taxonomy_internal', 'internal', $form_state->getValue('taxonomy_internal'));
  }

}
